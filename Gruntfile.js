var parseBuildPlatforms = function (argumentPlatform) {
	// this will make it build no platform when the platform option is specified
	// without a value which makes argumentPlatform into a boolean
	var inputPlatforms = argumentPlatform || process.platform + ";" + process.arch;

	// Do some scrubbing to make it easier to match in the regexes bellow
	inputPlatforms = inputPlatforms.replace("darwin", "mac");
	inputPlatforms = inputPlatforms.replace(/;ia|;x|;arm/, "");

	var buildAll = /^all$/.test(inputPlatforms);

	var buildPlatforms = {
		mac: /mac/.test(inputPlatforms) || buildAll,
		win: /win/.test(inputPlatforms) || buildAll,
		linux32: /linux32/.test(inputPlatforms) || buildAll,
		linux64: /linux64/.test(inputPlatforms) || buildAll
	};

	return buildPlatforms;
};

module.exports = function (grunt) {
	"use strict";

	var buildPlatforms = parseBuildPlatforms(grunt.option('platforms'));
	var pkgJson = grunt.file.readJSON('package.json');
	var currentVersion = pkgJson.version;

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('default', [
		'injectgit',
		'bower_clean',
		'nodewebkit',
		'shell:setexecutable'
	]);

    grunt.registerTask('build', [ 'default' ]);

	grunt.registerTask('dist', [
		'clean:releases',
		'clean:dist',
		'clean:update',
		'build',
		'exec:codesign', // mac
		'exec:createDmg', // mac
		'exec:createWinInstall',
		'exec:pruneProduction',
		'exec:createWinUpdate',
		'package' // all platforms
	]);

	grunt.registerTask('start', function () {
		var start = parseBuildPlatforms();
		if (start.win) {
			grunt.task.run('exec:win');
		} else if (start.mac) {
			grunt.task.run('exec:mac');
		} else {
			grunt.log.writeln('OS not supported.');
		}
	});

	grunt.registerTask('package', [
		'shell:packageWin',
		'shell:packageMac'
	]);

	grunt.registerTask('injectgit', function () {

		var gitRef, gitBranch, path, currCommit;
		
		if (grunt.file.exists('.git/')) {
			path = require('path');
			gitRef = grunt.file.read('.git/HEAD');
			try {
				gitRef = gitRef.split(':')[1].trim();
				gitBranch = path.basename(gitRef);
				currCommit = grunt.file.read('.git/' + gitRef).trim();

			} catch (e) {
				var fs = require('fs');
				currCommit = gitRef.trim();
				var items = fs.readdirSync('.git/refs/heads');
				gitBranch = items[0];
			}
			var git = {
				branch: gitBranch,
				commit: currCommit
			};
			grunt.file.write('.git.json', JSON.stringify(git, null, '  '));
		}
	});

	grunt.initConfig({

		nodewebkit: {
			options: {
				version: '0.9.2',
				build_dir: './build',
				keep_nw: true,
				embed_nw: false,
				mac_icns: './src/images/icon.png', // Path to the Mac icon file
				macZip: buildPlatforms.win, // Zip nw for mac in windows. Prevent path too long if build all is used.
				mac: buildPlatforms.mac,
				win: buildPlatforms.win,
				linux32: buildPlatforms.linux32,
				linux64: buildPlatforms.linux64,
				download_url: 'http://dl.node-webkit.org/'
			},
			src: ['./src/**',
				'./node_modules/**', '!./node_modules/bower/**', '!./node_modules/*grunt*/**',
				'!./**/test*/**', '!./**/doc*/**', '!./**/example*/**', '!./**/demo*/**', '!./**/bin/**', '!./**/build/**', '!./**/.*/**',
				'./package.json', './README.md', './LICENSE.txt', './.git.json'
			]
		},

		exec: {
			win: {
				cmd: '"build/cache/win/<%= nodewebkit.options.version %>/nw.exe" .'
			},
			mac: {
				cmd: 'build/cache/mac/<%= nodewebkit.options.version %>/node-webkit.app/Contents/MacOS/node-webkit .'
			},
			linux32: {
				cmd: '"build/cache/linux32/<%= nodewebkit.options.version %>/nw" .'
			},
			linux64: {
				cmd: '"build/cache/linux64/<%= nodewebkit.options.version %>/nw" .'
			},
			codesign: {
				cmd: 'sh dist/mac/codesign.sh || echo "Codesign failed, likely caused by not being run on mac, continuing"'
			},
			createDmg: {
				cmd: 'dist/mac/yoursway-create-dmg/create-dmg --volname "EVE Planetary ' + currentVersion + '" --window-size 480 540 --icon-size 128 --app-drop-link 240 370 --icon "EVE-Planetary" 240 110 ./build/releases/EVE-Planetary/mac/EVE-Planetary-' + currentVersion + '-Mac.dmg ./build/releases/EVE-Planetary/mac/ || echo "Create dmg failed, likely caused by not being run on mac, continuing"'
			},
			createWinInstall: {
				cmd: 'makensis dist/windows/installer_makensis.nsi',
				maxBuffer: Infinity
			},
			pruneProduction: {
				cmd: 'npm prune --production'
			}
		},
		shell: {
			setexecutable: {
				command: [
					'pct_rel="build/releases/EVE-Planetary"',
					'chmod -R +x ${pct_rel}/mac/EVE-Planetary.app || : '
				].join('&&')
			},
			packageWin: {
				command: [
					'cd build/releases/EVE-Planetary/win/EVE-Planetary',
					'tar --exclude-vcs -caf "../EVE-Planetary-' + currentVersion + '-Win.tar.xz" .',
					'echo "Windows Sucessfully packaged" || echo "Windows failed to package"'
				].join('&&')
			},
			packageMac: {
				command: [
					'cd build/releases/EVE-Planetary/mac/',
					'tar --exclude-vcs -caf "EVE-Planetary-' + currentVersion + '-Mac.tar.xz" EVE-Planetary.app',
					'echo "Mac Sucessfully packaged" || echo "Mac failed to package"'
				].join('&&')
			}
		},

		compress: {
			mac: {
				options: {
					mode: 'tgz',
					archive: 'build/releases/EVE-Planetary/mac/EVE-Planetary-' + currentVersion + '-Mac.tar.gz'
				},
				expand: true,
				cwd: 'build/releases/EVE-Planetary/mac/',
				src: '**',
				dest: ''
			},
			windows: {
				options: {
					mode: 'zip',
					archive: 'build/releases/EVE-Planetary/win/EVE-Planetary-' + currentVersion + '-Win.zip'
				},
				expand: true,
				cwd: 'build/releases/EVE-Planetary/win/EVE-Planetary',
				src: '**',
				dest: 'EVE-Planetary'
			}
		},

		clean: {
			releases: ['build/releases/EVE-Planetary/**'],
			dist: ['dist/windows/*.exe', 'dist/mac/*.dmg'],
			update: ['build/updater/*.*']
		}

	});

};
