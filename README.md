# [EVE Planetary](https://bitbucket.org/mike1pol/eve-planetary)

Windows & Mac program to information on Planetary on Eve Online

***

## Getting Involved

Want to report a bug, request a feature EVE Planetary? [Issues](https://bitbucket.org/mike1pol/eve-planetary/issues). We need all the help we can get!

## Getting Started

If you're comfortable getting up and running from a `git clone`, this method is for you.

If you clone the GitHub repository, you will need to build a number of assets using grunt.

#### Quickstart:

1. `npm install -g grunt-cli bower`
1. `bower install`
1. `npm install`
1. `grunt build`
1. `grunt start`

***

If you want to contact us: [mikle.sol@gmail.com](mailto:mikle.sol@gmail.com)
 
Copyright (c) 2014 EVE Planetary - Released under the [Apache license](https://bitbucket.org/mike1pol/eve-planetary/src/master/LICENSE.txt).
