var fs = require('fs');
var settings = JSON.parse(fs.readFileSync('src/settings.json').toString());
var resources = JSON.parse(fs.readFileSync('src/resources.json').toString());

$(document).ready(function () {
    // $.material.init();
    $(document).on('click', 'ul.planets li', function (event) {
        event.preventDefault();
        if ($('span', this).hasClass('active'))
            return false;
        $('ul.planets li span').removeClass('active');
        $('span', this).addClass('active');
        var id = parseInt($(this).attr('data-id'));
        htmlT(0, FindResource(id, 0, resources.resources), true);
    });

    $(document).on('mouseover', '.resources li', function () {
        if ($(this).data('selected'))
            return false;
        $(this).data('selected', true);
        overResource(parseInt($(this).attr('data-id')));
        $(this).one('mouseout', function () {
            $(this).data('selected', false);
            $('.resources li.btn-info').removeClass('btn-info');
        });
    });

    $(document).on('click', '.resources li', function() {
        var id = parseInt($(this).attr('data-id'));
        if (id > 0) {
            var obj = _.findWhere(resources.resources, {id: id});
            if (obj.t > 0) {
                $('#modalTitle').html(obj.name + ' (Tech ' + obj.t + ')');
                var html = '<iframe id="orgFrame" class="frame" src="frame.html"/>';
                $('#modalBody').html(html);
                $('#orgFrame').load(function() {
                    var org = '<li>' + obj.col + '<br><img src="./img/resources/' + obj.id +'.png" width="18"> ' + obj.name;
                    org += '<table width="100%" align="center" class="materials"><tr>'
                    for (var i in obj.res) {
                        org += '<td>' + obj.res[i].col + '</td>';
                    }
                    org += '</tr></table>';
                    org += backResourceHtml(id, true) + '</li>'
                    $('#orgFrame').contents().find('#org').html(org);
                    $('#orgFrame').contents().find('#org').jOrgChart({
                        chartElement: $('#orgFrame').contents().find('#orgData'),
                        depth: -1
                    });
                    $('#orgFrame').height($('#orgFrame').contents().find('#orgData').find('table').eq(0).outerHeight(true) + 50);
                });
                $('#modalWindow').modal('toggle');
            }
            else {
                $('#modalTitle').html(obj.name);
                var html = '<ul class="list-inline text-center">';
                for (var i in obj.res) {
                    var c = _.findWhere(resources.planets, { id: obj.res[i].id });
                    html += '<li><span class="planet planet-' + c.id + ' text-center"></span>' + c.name + '</li>'
                }
                html += '</ul>';
                $('#modalBody').html(html);
                $('#modalWindow').modal('toggle');
            }
        }
    });
});

var orgfullscreen = function() {
    if ($('#orgFrame').hasClass('fullscreen')) {
        $('#orgFrame').appendTo('#modalBody');
        $('#orgFrame').removeClass('fullscreen');
    }
    else {
        $('#orgFrame').appendTo('body');
        $('#orgFrame').addClass('fullscreen');
    }
};

var backResourceHtml = function(id, frist) {
    var html = '';
    var a = _.findWhere(resources.resources, {id: id});
    if (a.t > 0) {
        html += '<ul>';
        for (var b in a.res) {
            var c = _.findWhere(resources.resources, {id: a.res[b].id});
            if (c)
            {
                var col = '';
                if (c.col)
                    col = c.col + '<br>';
                html += '<li>' + col + '<img src="./img/resources/' + c.id +'.png" width="18"> ' + c.name;
                if (c.t > 0) {
                    html += '<br>Tech ' + c.t;
                    html += '<table width="100%" align="center" class="materials"><tr>'
                    for (var i in c.res) {
                        html += '<td>' + c.res[i].col + '</td>';
                    }
                    html += '</tr></table>';
                    html += backResourceHtml(c.id);
                }
                else
                    html += '<br>Resource'
                html += '</li>';
            }
        }
        html += '</ul>';
    }
    return html;
};

var overResource = function (id) {
    var a = _.findWhere(resources.resources, {id: id});
    overBackResource(id);
    var as = FindResource(id, a.t + 1, resources.resources);
    for (var i in as)
        $('.resources li[data-id=' + as[i].id + ']').addClass('btn-info');
};

var overBackResource = function(id) {
    var a = _.findWhere(resources.resources, {id: id});
    if (a && a.t && a.t > 0) {
        for (var b in a.res) {
            $('.resources li[data-id=' + a.res[b].id + ']').addClass('btn-info');
            overBackResource(a.res[b].id);
        }
    }
};

var htmlHide = function (s, e, act, cb) {
    if (act) {
        if (e > s) {
            cb();
            return false;
        }
        var obj = $('.resources[data-id=' + s + ']');
        s--;
        if (!obj.is(':hidden')) {
            obj.slideUp(function () {
                htmlHide(s, e, act, cb);
            });
        }
        else
            htmlHide(s, e, act, cb);
    }
};

var htmlT = function (t, arr, act) {
    var obj = $('.resources[data-id=' + t + ']');
    htmlHide(act === true ? 4 : 0, t, act, function () {
        var ids = [];
        $('ul', obj).html('');
        for (var i in arr) {
            $('ul', obj).append('<li data-id="' + arr[i].id + '" class="btn btn-default"><span class="iconr"><img src="./img/resources/' + arr[i].id +'.png"></span> ' + arr[i].name + '</li>');
            ids.push(arr[i].id);
        }
        obj.slideDown(function () {
            if (act && t < 4) {
                t++;
                var narr = [];
                for (var i in ids) {
                    narr = _.union(narr, FindResource(ids[i], t, resources.resources, ids));
                }
                if (narr.length > 0)
                    htmlT(t, narr, true);
            }
        });
    });
};

var FindResource = function (id, t, arr, ids) {
    return _.filter(_.where(arr, {t: t}), function (obj) {
        var r;
        if (t === 0 || !ids) {
            if (_.findWhere(obj.res, { id: id })) {
                r = true;
            }
        }
        else {
            var ln = [];
            for (var i in obj.res) {
                if (_.indexOf(ids, obj.res[i].id) !== -1)
                    ln.push(1);
            }
            if (obj.res.length == ln.length)
                r = true;
        }
        return r;
    });
};

var Materials = function(t) {
    $('.navbar-collapse.collapse li').removeClass('active');
    $('.navbar-collapse.collapse li.materials').addClass('active');
    $('.navbar-collapse.collapse li.materials li.t' + t).addClass('active');
    var html = '';
    if (t === 0) {
        for (i = 1; i < 5; i++) {
            html += '<div class="resources"><h2 class="text-center">Materials Tech ' + i + '</h2><ul class="list-inline text-center resources">';
            var res = _.where(resources.resources, { t: i });
            for (var r in res) {
                html += '<li data-id="' + res[r].id + '" class="btn btn-default"><span class="iconr"><img src="./img/resources/' + res[r].id +'.png"></span> ' + res[r].name + '</li>';
            }
            html += '</div><div class="clearfix"></div>';
        }
    }
    else {
        html += '<h2 class="text-center">Materials Tech ' + t + '</h2><ul class="list-inline text-center resources">';
        var res = _.where(resources.resources, { t: t });
        for (var i in res) {
            html += '<li data-id="' + res[i].id + '" class="btn btn-default"><span class="iconr"><img src="./img/resources/' + res[i].id +'.png"></span> ' + res[i].name + '</li>';
        }
        html += '</ul>';
    }
    $('#page').html(html);
};

var Resources = function() {
    $('.navbar-collapse.collapse li').removeClass('active');
    $('.navbar-collapse.collapse li.resources').addClass('active');
    var html = '<h2 class="text-center">Resources</h2><ul class="list-inline text-center resources">';
    var res = _.where(resources.resources, { t: 0 });
    for (var i in res) {
        html += '<li data-id="' + res[i].id + '" class="btn btn-default"><span class="iconr"><img src="./img/resources/' + res[i].id +'.png"></span> ' + res[i].name + '</li>';
    }
    html += '</ul>';
    $('#page').html(html);
};

var Planets = function () {
    $('.navbar-collapse.collapse li').removeClass('active');
    $('.navbar-collapse.collapse li.planets').addClass('active');
    var i;
    var planets = '<h2 class="text-center">Planets</h2><ul class="list-inline text-center planets">';
    for (i = 0; i < resources.planets.length; i++) {
        planets += '<li data-id="' + resources.planets[i].id + '"><span class="planet planet-' + resources.planets[i].id + '"></span>' + resources.planets[i].name + '</li>';
    }
    planets += '</ul><div class="clearfix"></div>';
    for (i = 0; i < 5; i++) {
        planets += '<div class="resources" data-id="' + i + '" style="display:none">';
        if (i === 0)
            planets += '<h4 class="text-center">Resources</h4>';
        else
            planets += '<h4 class="text-center">Tech ' + i + '</h4>';
        planets += '</h4><ul class="list-inline text-center"></ul>';
        planets += '</div><div class="clearfix"></div>';
    }
    $('#page').html(planets);
};

Planets();

// fs.watch('./', function () {
//     if (location)
//         location.reload();
// });